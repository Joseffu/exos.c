#include <stdio.h> // printf(), scanf()
#include <stdlib.h> // EXIT_SUCCESS, atoi(), rand()
#include <time.h> // time()

/** lecture d'un argument passé par la ligne de commande.
 * Note : pas fool-proof du tout car flemme de vérifier tous les cas dans un exo. 
 */
int main(int argc, char** argv) {
    
    (void)argc; // indication au compilateur qu'on sait bien que l'argument argc n'est pas utilisé

    int nb_essais_og; // nombre d'essais pour deviner le nombre
    
    nb_essais_og = atoi(argv[1]); // conversion de la chaîne passée en param en nombre

    // impression d'un entier dans le terminal
    printf("Devinez un entier inclus dans [0, 100] en %d essai(s).\n", nb_essais_og);

    // détermination aléatoire du nombre à deviner
    srand(time(NULL)); // seed selon la seconde actuelle
    int nb = rand() % 101; // entier aléatoire dans [0,100]

    /** 
     * boucle : tant qu'il reste des essais ou que le joueur a deviné le bon nombre  
    */

    int nb_essais = 0; // nombre d'essais effectués

    while(nb_essais < nb_essais_og){
        printf("\n -> %d essai(s) restant. Votre suggestion ? ", nb_essais_og - nb_essais);

        /** lecture du nombre essayé */
        int essai; 
        scanf("%d", &essai); // lecture de l'essai depuis le terminal

        /** verification bon ou mauvais essai */

        // bon essai
        if(essai == nb){
            // bon essai. Le joueur a gagné ! Fin du prog
            printf("\n[O] Bien joué ! c'était effectivement %d. Vous avez réussi en %d essais.",
                nb, nb_essais
            );
            // fin du prog avec succes
            return EXIT_SUCCESS;
        }
        else { // mauvais essai
            printf("Non, ce n'est pas %d ...\n", essai);
            // indice 
            if(essai < nb) printf("Le nombre à deviner est plus (+) grand\n");
            else printf("Le nombre à deviner est moins (-) grand\n");
        }

        /** 1 essai comptabilisé */
        nb_essais += 1;
    }

    // fin des essais : le joueur a perdu !
    printf("\n[X] Vous avez utilisé tous vos essais. Perdu :/ \n");
    printf("Le bon nombre était %d", nb);

    /** 
     * là aussi fin du prog avec succès (ne pas confondre fin du jeu avec fin de l'exec d'un prog)
     */
    return EXIT_SUCCESS; 
}