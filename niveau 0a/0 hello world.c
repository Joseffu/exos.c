/** 
 * inclusion de bibliothèques 
 */
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS

/**
 * point d'entrée des programmes C : fonction main avec renvoit d'un entier 
 */
int main(){
    // impression d'une chaîne de caractères dans la sortie habituelle (terminal du programme)
    printf("Hello World. C'est turing qui a crée le meme de hello world ? bref, chaîne de caractères avec escapade et retour chariot tel que '\\n'.\n");
    
    // fin du programme : retour du code de fin du prog
    printf("Fin exec du premier programme.");
    return EXIT_SUCCESS; // renvoit du code de fin avec succès (int 0)
}
