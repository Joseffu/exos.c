/** @file Fichier point d'entrée du programme de ray tracing.
 * @note Code le plus basique possible. Flemme de coder un programme complètement 'fool-proof' ou qui 'free' chaque 'malloc'. Tant que ça fonctionne correctement ça me suffit.  
 */

#include <stdio.h> // printf()
#include <stdlib.h> // EXIT_SUCCESS

#include "src/vec3.h" // struct vec3

#include "src/init.c" // fonctions d'initialisations de données
#include "src/unit_tests.c" // fonctions de tests unitaires
#include "src/scenes.c" // fonctions de construction des scènes à calculer  

/** _Paramètres du programmes */
int _width; /// largeur de l'image finale calculée, en pixels 
int _height; /// hauteur de l'image finale calculée, en pixels 
int _PPR; /// nombre de lancés de rayon par pixel (Pixel Per Ray)

/** @brief Point d'entrée du programme de 'ray tracing' simple en C.
 * @note Tentative de 'débloater' au max le code que j'écris. A voir si openAcc fonctionne pour les fonction récursive de 'raytracing'
 * @note Arguments du terminal : largeur de l'image, hauteur, nombre de rayons lancé par pixel, nom du fichier image résultat
 */   
int main(int argc, char **argv){
    /** vérification 'fool-proof' basique des arguments */

    if(argc != 5){
        printf("usage: %s <largeur> <hauteur> <nombre de lancés par pixel> <nom_fichier_sortie>\n", argv[0]);
        return EXIT_FAILURE;
    }

    /** Lecture des paramètres du programme */

    // lecture des paramètres de l'image finale calculée
    _width = atoi(argv[1]);
    _height = atoi(argv[2]);
    _PPR = atoi(argv[3]); 
    printf("--> Paramètres : image %d x %d, %d rayons lancés par pixel.\n",
        _width, _height, _PPR
    );

    /** Initialisations */

    init_all(); // fonctions qui regroupe tous les 'init' à faire

    /** tests unitaires de débuggage */

    printf("\n--> Exécution des tests unitaires...\n");
    if(run_all_unit_tests()){
        printf("-> Tests unitaires terminés avec succès.\n");
    }
    else {
        printf("--> ERREUR tests unitaires...\n");
        return EXIT_FAILURE;
    }

    /** 'Setup' de la scène à 'render' */
    choose_scene();



    return EXIT_SUCCESS;
}