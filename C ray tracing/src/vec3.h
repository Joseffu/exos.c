///@file Définition et opérations sur les vecteurs de R^3

#ifndef VEC3_H
#define VEC3_H

#include <stdlib.h> // malloc()
#include <stdio.h> // printf()

/** @brief Définition de la structure vec3.
*/ 
typedef struct _vec3 {
    //composantes enregistrées dans un tableau
    float e[3];
} vec3;

/** @brief Constructeur vec3 basique : sans paramètre.
 * @note Composantes du vecteur définies sur R (float). 
 * @return pointeur sur la nouvelle structure allouée.
 * @warning Ne pas oublier de free ce malloc.
 */
vec3 * vec3_alloc() { 
    vec3 *v = (vec3*)malloc(sizeof(vec3)); // allocation mémoire
    v->e[0] = v->e[1] = v->e[2] = 0; // initialisation à (0,0,0) par défaut 
    return v;
}

/** @brief Constructeur vec3 avec paramètre.
 * @note Composantes du vecteur définies sur R (float). 
 * @return pointeur sur la nouvelle structure allouée.
 */
vec3 * vec3_new(float e0, float e1, float e2) { 
    vec3 *v = vec3_alloc();
    v->e[0] = e0; v->e[1] = e1; v->e[2] = e2;  
    return v;
}

/** @brief Affichage des valeurs des composantes du vecteur dans le terminal.
 * @param v Pointeur vers le vec3 à print.
 * @param s (char*) pointeur vers une string à afficher avec ce vec3. Peut être NULL et ne sera pas affiché dans ce cas.
 */
void vec3_print(vec3 *v, char *s){ 
    if(s != NULL) { printf("%s: ", s); }
    printf("%f, %f, %f.\n", v->e[0], v->e[1], v->e[2] ); 
}
    
#endif //VEC3_H
