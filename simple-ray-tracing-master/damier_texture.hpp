///@file texture damier : quadrillage comme le jeu de dame, alternance de carrées de 2 couleurs différentes

#ifndef DAMIER_TEXTURE_H
#define DAMIER_TEXTURE_H

#include "texture.hpp"

class damier_texture : public texture {

public:
    // textures des 2 types de carreaux du damier de l'objet
    texture* couleur_a; 
    texture* couleur_b;
    // largeur des carreaux
    float largeur_carreau;

    //constructeur
    damier_texture(texture* couleur_a_, texture* couleur_b_, float largeur_carreaux_) { 
        couleur_a = couleur_a_; 
        couleur_b = couleur_b_; 
        largeur_carreau = 1.0 / largeur_carreaux_;
    }

    // voir texture::valeur pour plus de détails
    virtual vec3 valeur(const intersec_data& inter_data) const {
        //damier délimité depuis les sinus de la position (alternance des valeurs positives et négatives)
        float alternance = 
            sin(largeur_carreau * inter_data.position.x())
            * sin(largeur_carreau * inter_data.position.y())
            * sin(largeur_carreau * inter_data.position.z());
        
        //float alternance = sin(largeur_carreau * (inter_data.position.x() + inter_data.position.y() + inter_data.position.z()));
        
        /*
        float alternance = 
        sin(largeur_carreau * inter_data.position.x())
            + sin(largeur_carreau * inter_data.position.y())
            + sin(largeur_carreau * inter_data.position.z());
        */
        return alternance > 0 
            ? couleur_a->valeur(inter_data)
            : couleur_b->valeur(inter_data);
    }
};

#endif //DAMIER_TEXTURE_H