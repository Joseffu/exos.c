#!/bin/bash

echo "* nettoyage des fichiers précédents"
rm jeu_dichotomique

echo "
* compilation du prog"
gcc '1 jeu dichotomique.c' -o jeu_dichotomique -Wall -Wextra

echo "
* excecution du prog : jeu de devinette d'un entier compris dans [0, 100] en 10 essai max... 
"
echo "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"
./jeu_dichotomique 10
echo "
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"

echo "

* FIN exec run.sh"
