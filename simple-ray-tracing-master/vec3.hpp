///@file Définition et opérations sur vecteurs de R^3

#ifndef VEC3_H
#define VEC3_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h> //drand48

class vec3 {

public: 
    //composantes enregistrées dans un tableau
    float e[3];
    //constructeur basique
    vec3() { e[0] = e[1] = e[2] = 0; }
    //constructeur depuis les composantes
    vec3(float x, float y, float z) { e[0] = x; e[1] = y; e[2] = z; }
    
    //inline = optimisation 
    inline float x() const { return e[0]; }
    inline float y() const { return e[1]; }
    inline float z() const { return e[2]; }

    //opérations unaires
    inline vec3 operator-() const { return vec3(-e[0], -e[1], -e[2]); }

    //norme du vecteur
    inline float norme() const { return sqrt(e[0]*e[0] + e[1]*e[1] + e[2]*e[2]); }

    //racine de la norme d'un vecteur
    //note : utilisé pour check si un vecteur a une norme < 1
    inline float racine_norme() const { return e[0]*e[0] + e[1]*e[1] + e[2]*e[2]; }

    inline void print(const char* s) const { printf("%s: %f, %f, %f.\n",s, e[0], e[1], e[2]); }

};

//addition entre 2 vecteurs
inline vec3 operator+(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.e[0] + v2.e[0], v1.e[1] + v2.e[1], v1.e[2] + v2.e[2]);
}

//multiplication avec un scalaire
inline vec3 operator*(float t, const vec3 &v) {
    return vec3(t*v.e[0], t*v.e[1], t*v.e[2]);
}

//division avec un scalaire
inline vec3 operator/(vec3 v, float t) {
    return vec3(v.e[0]/t, v.e[1]/t, v.e[2]/t);
}

//soustraction entre 2 vecteurs
inline vec3 operator-(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.e[0] - v2.e[0], v1.e[1] - v2.e[1], v1.e[2] - v2.e[2]);
}

//produit scalaire entre 2 vecteurs
inline float prod_scal(const vec3 &v1, const vec3 &v2) {
    return v1.e[0] *v2.e[0] + v1.e[1] *v2.e[1]  + v1.e[2] *v2.e[2];
}

//vecteur directeur normalisé
inline vec3 vect_unit(vec3 v) {
    return v / v.norme();
}

//multipliation entre 2 veteurs
// ATTENTION, différent du produit scalaire / vectoriel
inline vec3 operator*(const vec3 &v1, const vec3 &v2) {
    return vec3(v1.e[0] * v2.e[0], v1.e[1] * v2.e[1], v1.e[2] * v2.e[2]);
}

/** @brief Renvoi un vecteur dans la sphère unitaire (vecteur aléatoire de norme <= 1)
 */
inline vec3 vect_alea_dans_sphere_unitaire(){
    vec3 v;
    do {
        //valeur de chaque composante aléatoirement entre 0 et 2
        v = 2.0 * vec3(drand48(), drand48(), drand48()) - vec3(1,1,1);
    } while( v.racine_norme() >= 1 ); // tant qu'on est pas dans la sphère unitaire
    //note: on utilise la racine de la norme car plus rapide à calculer

    return v;
}

/** @brief Produit vectoriel entre 2 vecteurs
 */
inline vec3 prod_vect(const vec3 &v1, const vec3 &v2) {
    return vec3( (v1.e[1]*v2.e[2] - v1.e[2]*v2.e[1]),
                (-(v1.e[0]*v2.e[2] - v1.e[2]*v2.e[0])),
                (v1.e[0]*v2.e[1] - v1.e[1]*v2.e[0]));
}

#endif //VEC3_H
