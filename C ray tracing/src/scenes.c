/** @file Fichier contenant les scènes à mettre en place 
 */

#include <stdio.h> // printf(), scanf(), 

/** @brief 'Setup' de la scène 1 : les sphères.
 */
void setup_scene_spheres(){
    printf("--> Setup de la scène 1 : les sphères...\n");


}

/** @brief Lecture du choix de l'utilisateur de la scène à calculer.
 */ 
void choose_scene(){

    /* lecture du choix depuis le terminal */
    printf("\n--> Choisissez la scène à calculer :\n");
    printf("1. Scène de sphères.\n");

    printf("Choix ? "); 
    int choice;
    if(scanf("%d", &choice) == EOF){
        printf("ERREUR choose_scene() : erreur lecture scanf() du choix; invalide.");
        exit(EXIT_FAILURE); // fin du programme avec le code d'erreur.
    }
    printf("\n");

    switch (choice)
    {
    case 1:
        setup_scene_spheres();
        break;
    
    default:
        printf("ERREUR choose_scene() : choix de la scène invalide.\n");
        exit(EXIT_FAILURE);
        break;
    }

}

