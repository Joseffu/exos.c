//@file metal mat = couleur + lumière réfléchie sur la surface (avec une certaine préciision -> floutage de la reflection)
#ifndef METAL_MAT_H
#define METAL_MAT_H

#include "material.hpp"

/** @brief Calcule le vecteur réfléchi après un vecteur rebondissant sur une surface
 * @param rayon_entrant Le vecteur du rayon qui rebondi sur la surface
 * @param normale Normale de la surface sur lequel le rayon a rebondi
 */
vec3 rebondissement(const vec3& rayon_entrant, const vec3& normale){
    return rayon_entrant - 2 * prod_scal(rayon_entrant, normale) * normale;
}

class metal_mat : public material {
public :

    //TODO: changer le vec3 albedo par une texture albedo
    vec3 albedo; //couleur de l'objet (sa façon d'absorber la lumière)
    float floutage; // floutage de la réflection (metal non lisse). 0 = parfaitement lisse.
    
    //constructeur
    metal_mat(const vec3& couleur, float flou) { 
        albedo = couleur; 

        if( flou < 0 ) floutage = 0;
        else if (flou > 1) floutage = 1;
        else floutage = flou; 
    }
    //voir material::calcul_impacte pour plus de détails
    virtual bool calcul_impacte(
        const rayon& rayon_entrant, const intersec_data& inter_data, 
        vec3& attenuation, rayon& rayon_sortant) const {

            //direction du rayon après rebondissement:
            // rayon entrant rebondit parfaitement (réflection)
            vec3 nouvelle_direction = rebondissement(
                rayon_entrant.direction(), 
                inter_data.normale
            );
            rayon_sortant = rayon(
                inter_data.position, 
                nouvelle_direction + floutage * vect_alea_dans_sphere_unitaire()
            );
            
            //on atténue la luminosité selon la couleur du material
            attenuation = albedo;
            return prod_scal(rayon_sortant.direction(), inter_data.normale) >= 0;

    }
};

#endif // METAL_MAT_H
