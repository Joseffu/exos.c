///@file Rectangle d'un plan parallèle au plan xZ
#ifndef RECT_XZ_H
#define RECT_XZ_H

#include "objet.hpp"
#include "rayon.hpp"
#include "material.hpp"

///le rectangle (morceau de plan) est un objet de la scene
class rect_xz: public objet {
public:
    
    //coordonnées du plan
    float y; //position du plan sur l'axe Y 

    float x0, x1, z0, z1; //bords du plan
    //note : pour éviter d'inverser l'orientation du plan x0 < x1 et z0 < z1

    material * mat; //material du rectangle

    //constructeur basique
    rect_xz() {};

    /** @brief Constructeur complet
     * @param x0_ bords gauche
     * @param x1_ bords droit
     * @param z0_ bords bas
     * @param z1_ bords haut
     * @note x0 et x1 ainsi que z0 et z1 peuvent être inversés si x0 > x1 ou z0 > z1
     */ 
    rect_xz(float x0_, float x1_, float z0_, float z1_, float y_, material* mat_) {
        /*
        if(x0_ == y0_ && x1_ == y1_){
            printf("ERREUR: rect_xy : les 2 points du plans doivent être différents !\n");
            exit(EXIT_FAILURE);
        }
        */
        

        /*
        x0 = x0_; x1 = x1_;
        z0 = z0_; z1 = z1_;
        */
       
        x0 = x0_ < x1_ ? x0_ : x1_;
        x1 = x0_ < x1_ ? x1_ : x0_;

        z0 = z0_ < z1_ ? z0_ : z1_;
        z1 = z0_ < z1_ ? z1_ : z0_;
        
        y = y_;

        centre = vec3((x0 + x1)/2.0, y, (z0 + z1)/2.0);

        float offset = 0.05; // marge coins bounding volume
        coin_min = vec3(x0 - offset, y - offset, z0 - offset);
        coin_max = vec3(x1 + offset, y + offset, z1 + offset);

        mat = mat_;
    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool rect_xz::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //calcul de l'éventuelle intersection entre le rayon et le plan

    //si la direction du rayon est parallèle au plan alors aucune intersection
    if(ray.direction().y() == 0){
        return false;
    }

    //on calcule le paramètre t pour le point d'intersection
    float t = (y - ray.origine().y()) / ray.direction().y();
    
    //si la distance n'est pas dans les bornes acceptables 
    if( t < dist_min || t > dist_max){
        return false;
    }

    //on calcule les coordonnées x et y du point dans ce plan
    float x = ray.origine().x() + t * ray.direction().x();
    float z = ray.origine().z() + t * ray.direction().z();

    //on regarde si les coordonnées sont dans les bornes du rectangle du plan
    if(x0 <= x && x <= x1 && z0 <= z && z <= z1){
        inter_data.dist = t;
        inter_data.position = vec3(x,y,z);
        inter_data.normale = vec3(0,1,0);
        inter_data.ptr_material = mat;
        return true;
    }   
    else {
        return false;
    }
   
}

#endif //RECT_XZ_H

