///@file objet contenant les informations liées à lintersection entre un rayon de rendu et un objet de la scène

#ifndef INTERSEC_DATA_H
#define INTERSEC_DATA_H

#include "vec3.hpp"
#include "material.hpp"

class material; //pour les références circulaires

///Stockage des données en lien avec l'intersection avec un rayon de rendu
struct intersec_data {
    float dist; ///distance de l'origine du rayon au point d'intersection
    vec3 position; ///point d'intersection dans l'espace
    vec3 normale; ///normale de l'objet visible au point d'intersection 
    material *ptr_material; //pointeur vers le materiau de l'objet qui a été touché par le rayon de rendu
};

#endif //INTERSEC_DATA_H