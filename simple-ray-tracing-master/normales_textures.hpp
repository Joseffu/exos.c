///@file texture normales : couleur en fonction de la direction de la normale (couleur unique pour chacune des directions)
#ifndef NORMALES_TEXTURE_H
#define NORMALES_TEXTURE_H

#include "texture.hpp"

class normales_texture : public texture {

public:


    //constructeur
    normales_texture() { }

    // voir texture::valeur pour plus de détails
    virtual vec3 valeur(const intersec_data& inter_data) const {
        //couleur du pixel selon la normale de l'objet au point d'intersection
        return 0.5 * vec3(inter_data.normale.x() +1, inter_data.normale.y() +1, inter_data.normale.z() +1);
    }
};

#endif // NORMALES_TEXTURE_H