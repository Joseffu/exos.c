/** @file Fichier contenant les fonctions de tests unitaires 
 */

#include <stdbool.h> // bool

#include "vec3.h" // struct vec3

/** @brief Applique tous les tests unitaires.
 * @note Tous les tests possibles ne sont pas effectués, je code pour le fun, pas pour une entreprise. Je teste seulement les éléments qui me paraissent ne pas fonctionner. 
 * @return Booléen vrai si les tests ont été effectué avec succès.
 */
bool run_all_unit_tests(){
    bool valid = true;

    // test de l'allocation struct vec3
    vec3 * vecteur = vec3_new(7, 42, 1337.01);
    vec3_print(vecteur, "test struct vec3");

    // test de l'initialisation de la couleur BLANC
    vec3_print(BLANC, "Valeur de vec3 BLANC : ");
    vec3_print(GOLD_ALBEDO, "Valeur de vec3 GOLD_ALBEDO : ");
    
    return valid;
}