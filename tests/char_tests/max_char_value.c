#include <stdio.h> // printf()

/**
 * test de ce qu'il se passe avec les dépassements des valeurs max du datatype char
*/

/** affiche des octets sous forme de string de 0 et 1 
 * note : écriture en texte des octets de droite à gauche (bit de poids faible à droite)
*/
void print_bytes_to_bits_string(char *pointer, int number_of_bytes){
   // printf("adresse pointeur : %d \n", (int)pointer); // _DEBUG_
   // printf("valeur pointée : %d \n", (int)*pointer); // _DEBUG_
   
   // pour chaque octet
   for(int B = number_of_bytes - 1; B >= 0; B--){
      // pour chaque bit dans l'octet (bit de poids faible à droite donc on print d'abords le bit de poids fort)
      for(int b = 7; b >= 0; b--){

         if(b == 3) printf(" "); // j'aime bien séparer les bits par groupe de 4 bits

         // affichage du bit
         if( ( (char) *(pointer+B)) & (1 << b) ) 
            printf("1");
         else 
            printf("0");
      }

      printf("  "); // j'aime bien séparer chaque octet
   }
}

int main(){
   /** test sur la mémoire d'une var type char */   
   printf("*** data type char *** \n\n");
   char char_var; // data type char : caractère ascii encodé sur 8 bits = 1 octet
   printf("-> taille d'une variable char (nombre d'octets) : %ld \n", sizeof(char_var));
   printf("-> %ld octet(s) donc constitué de 8x plus de bits : %ld bits \n",
      sizeof(char_var), 8*sizeof(char_var)
   );

   /** test écriture d'un char (octet) bit par bit */
   char_var = 42;
   printf("-> affichage des bits d'une variable char contenant %d : ", char_var);
   print_bytes_to_bits_string(&char_var, sizeof(char_var));

   /** test valeur max d'une variable 'char' (donc signée) */
   int wrong_min_char, wrong_max_char;
   wrong_min_char = -130; 
   wrong_max_char = 130;

   printf("\n\n-> affichage des valeurs hors bornes d'une variable 'char' (signée) : [%d, %d]... \n",
      wrong_min_char, wrong_max_char
   );
   for(int i = wrong_min_char; i <= wrong_max_char; i++){
      char_var = i;
      printf("%d (base 10) : ", i); 
      print_bytes_to_bits_string(&char_var, sizeof(char_var)); printf(" (base 2)\n");
   }

   printf("--> on peut remarquer que le type 'char' ne peut contenir que 256 valeurs d'entier différentes : [-127, +127] (ne pas oublier la valeur 0).\n");
   printf("--> exemple : 130 (10) qui est hors borne devient -126 (10) = 1000 0010 (2) qui lui est dans les bornes définies par 'char' (en langage c).\n");
   printf("--> Il y a une surjection de 'int' vers 'char' : \"boucle périodique de redéfinition\" limitée par -128 (10) = +128 (10) = 1000 0000 (2). (Smah les mathématiciens j'ai pas le vocabulaire ni la compréhension d'un universitaire. Free as beer. Intuitif comme X jusqu'à que je puisse me payer un prof particulier.)"); 
   //TODO: apprendre le vocabulaire des notions mathématiques permettant d'exprimer convenablement les notions sur lesquelles je m'entraîne.
   
   /** test affichage des bits d'un int */

   int signed_int = 1024 + 1 ;
   printf("\n\n-> affichage des bits d'un int = %d (10) en base (2) : ", signed_int);
   print_bytes_to_bits_string((char*)&signed_int, sizeof(signed_int));
   
   signed_int = -1;
   printf("\n-> affichage des bits d'un int = %d (10) en base (2) : ", signed_int);
   print_bytes_to_bits_string((char*)&signed_int, sizeof(signed_int));
   // TODO: vérifier que l'encodage d'un signed int négatif correspond au resultat que me rend cette focntion 
   
   return 1;
 }