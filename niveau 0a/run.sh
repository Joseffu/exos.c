#!/bin/bash

#exécution d'un fichier scrip bash. Première ligne "commentaire" importante 
#exécuter un fichier bash nécessite de lui avoir auparavent donné les droits : chmod +x rush.sh 

echo "* nettoyage des fichiers précédents"
rm helloworld

echo "
* compilation du prog"
gcc '0 hello world.c' -o helloworld -Wall -Wextra

echo "
* excecution prog : un hellow world tout simple...
"
echo "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"
./helloworld
echo "
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"

echo "

* FIN exec run.sh"
