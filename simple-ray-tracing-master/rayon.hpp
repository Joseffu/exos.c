#ifndef RAYON_H
#define RAYON_H

#include "vec3.hpp"

#include <stdio.h>
#include <math.h>

class rayon {

public: 
    vec3 A; //origine du rayon
    vec3 B; // direction du rayon

    //constructeur basique
    rayon() {}
    //constructeur : A = point d'origine, B = direction
    rayon(const vec3& a, const vec3& b) { A = a; B = b; }

    vec3 origine() const {return A;}
    vec3 direction() const { return B;}

    // rayon défini par l'eq paramétrique p(t) = origine + t * direction 
    vec3 lancer(float t) const { return A + t * B;}
};


#endif //RAYON_H
