///@file texture uniforme : même couleur peu importe le point d'intersection
#ifndef UNIFORM_TEXTURE_H
#define UNIFORM_TEXTURE_H

#include "texture.hpp"

class uniform_texture : public texture {

public:

    vec3 couleur; // couleur de l'objet

    //constructeur
    uniform_texture(vec3 couleur_) { couleur = couleur_; }

    // voir texture::valeur pour plus de détails
    virtual vec3 valeur(const intersec_data& inter_data) const {
        //on renvoit toujours la même couleur peu importe le point d'intersection
        return couleur;
    }
};

#endif // UNIFORM_TEXTURE_H