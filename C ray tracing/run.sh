#!/bin/bash

echo " ------ nettoyage ------"
rm c_raytracing.out
rm image_resultat.ppm

echo " ------ compilation ------"
gcc -O3 main.c -o c_raytracing.out -g -lm -Wall -Wextra -Werror

echo " ------ exécution ------"
./c_raytracing.out 1000 500 5 image_resultat.ppm

echo " ------ ouverture image ------ "
xdg-open image_resultat.ppm
