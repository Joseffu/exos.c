///@file Objet de la scène : une sphère

#ifndef SPHERE_H
#define SPHERE_H

#include "objet.hpp"
#include "rayon.hpp"
#include "material.hpp"

///sphere est un objet de la scene
class sphere: public objet {
public:
    float rayon_; //_ ajouté pour ne pas avoir le même nom que la classe rayon (rayon de rendu)... 
    //vec3 coin_min, coin_max; //coin des bornes de collision 

    //constructeur basique
    sphere() {};
    /** @brief Constructeur complet
     * @param c Centre de la sphere
     * @param r Rayon de la sphere
     * @param m Material de la surface de la sphère
     */ 
    sphere(vec3 c, float r, material* m) {
        centre = c; 
        rayon_ = r; 
        mat = m; 

        //calcul bornes boite collision
        float offset = 0.1;
        r += offset;
        coin_min = vec3( c.x() - r, c.y() - r, c.z() - r);  
        coin_max = vec3( c.x() + r, c.y() + r, c.z() + r);    
    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool sphere::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //calcul de l'éventuelle intersection
     
    //dist = vecteur pour aller de l'origine du rayon au centre du cercle
    vec3 dist = ray.origine() - centre;
    //calcul determinant equation a*t^2 + b*t + c = 0
    float a = prod_scal(ray.direction(), ray.direction());
    float b = 2.0 * prod_scal(ray.direction(), dist);
    float c = prod_scal(dist, dist) - rayon_ * rayon_;
    float discriminant = b*b - 4.0 * a * c;
    //si rayon transperse le cercle
    if(discriminant > 0){
        //on verifie que le point d'intersection est dans l'intervalle de distance accepté 
        //note: c'est pour déterminer la couleur de l'objet le plus proche
        
        // 1 ere intersection
        float dist_inter = (-b - sqrt(b*b - 4*a*c)) / 2*a;
        if ( dist_min < dist_inter && dist_inter < dist_max) {
            //dans l'intervalle : on calcul le point d'inter et la normale
            inter_data.dist = dist_inter;
            inter_data.position = ray.lancer(dist_inter);
            inter_data.normale = (inter_data.position - centre) / rayon_;
            inter_data.ptr_material = mat;

            return true;
        }

        // 2eme intersection si 1ere invalide (pas dans l'intervalle)
        dist_inter =  (-b + sqrt(b*b - 4*a*c)) / 2*a;
        if ( dist_min < dist_inter && dist_inter < dist_max) {
            //dans l'intervalle : on calcul le point d'inter et la normale
            inter_data.dist = dist_inter;
            inter_data.position = ray.lancer(dist_inter);
            inter_data.normale = (inter_data.position - centre) / rayon_;
            inter_data.ptr_material = mat;
            
            return true;
        }
    }
    return false;
}

#endif //SPHERE_H

