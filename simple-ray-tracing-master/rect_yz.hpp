///@file Rectangle d'un plan parallèle au plan yz
#ifndef RECT_YZ_H
#define RECT_YZ_H

#include "objet.hpp"
#include "rayon.hpp"
#include "material.hpp"

///le rectangle (morceau de plan) est un objet de la scene
class rect_yz: public objet {
public:
    
    //coordonnées du plan
    float x; //position du plan sur l'axe X 

    float y0, y1, z0, z1; //bords du plan
    //note : pour éviter d'inverser l'orientation du plan x0 < x1 et z0 < z1

    material * mat; //material du rectangle

    //constructeur basique
    rect_yz() {};

    /** @brief Constructeur complet
     * @param y0_ bords gauche
     * @param y1_ bords droit
     * @param z0_ bords bas
     * @param z1_ bords haut
     * @note y0 et y1 ainsi que z0 et z1 peuvent être inversés si y0 > y1 ou z0 > z1
     */ 
    rect_yz(float y0_, float y1_, float z0_, float z1_, float x_, material* mat_) {
        /*
        if(x0_ == y0_ && x1_ == y1_){
            printf("ERREUR: rect_xy : les 2 points du plans doivent être différents !\n");
            exit(EXIT_FAILURE);
        }
        */
       
        y0 = y0_ < y1_ ? y0_ : y1_;
        y1 = y0_ < y1_ ? y1_ : y0_;

        z0 = z0_ < z1_ ? z0_ : z1_;
        z1 = z0_ < z1_ ? z1_ : z0_;
        /* 
        z0 = z0_; z1 = z1_;
        y0 = y0_; y1 = y1_;
        */
        x = x_;

        centre = vec3(x, (y0 + y1)/2.0, (z0 + z1)/2.0);

        float offset = 0.05; // marge coins bounding volume
        coin_min = vec3(x - offset, y0 - offset, z0 - offset);
        coin_max = vec3(x + offset, y1 + offset, z1 + offset);

        mat = mat_;
    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};

bool rect_yz::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //calcul de l'éventuelle intersection entre le rayon et le plan

    //si la direction du rayon est parallèle au plan alors aucune intersection
    if(ray.direction().x() == 0){
        return false;
    }

    //on calcule le paramètre t pour le point d'intersection
    float t = (x - ray.origine().x()) / ray.direction().x();
    
    //si la distance n'est pas dans les bornes acceptables 
    if( t < dist_min || t > dist_max){
        return false;
    }

    //on calcule les coordonnées x et y du point dans ce plan
    float y = ray.origine().y() + t * ray.direction().y();
    float z = ray.origine().z() + t * ray.direction().z();

    //on regarde si les coordonnées sont dans les bornes du rectangle du plan
    if(y0 <= y && y <= y1 && z0 <= z && z <= z1){
        inter_data.dist = t;
        inter_data.position = vec3(x,y,z);
        inter_data.normale = vec3(1,0,0);
        inter_data.ptr_material = mat;
        return true;
    }   
    else {
        return false;
    }
   
}

#endif //RECT_YZ_H

