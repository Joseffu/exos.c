#include "vec3.h" // couleurs vec3

/** Définition de certaines couleurs */

vec3 *BLANC;
vec3 *BLEU;
vec3 *ROUGE;
vec3 *VERT;
vec3 *NOIR;
vec3 *GRIS;

vec3 *TURQUOISE;
vec3 *FUSHIA;
vec3 *ROSE;
vec3 *BLEU_CIEL;
vec3 *JAUNE;
vec3 *GOLD_ALBEDO;

void init_colors(){
    BLANC = vec3_new(1.0, 1.0, 1.0);
    BLEU = vec3_new(0.0, 0.0, 1.0);
    ROUGE = vec3_new(1.0, 0.0, 0.0);
    VERT = vec3_new(0.0, 1.0, 0.0);
    NOIR = vec3_new(0,0,0);
    GRIS = vec3_new(0.5, 0.5, 0.5);

    TURQUOISE = vec3_new(0., 255, 188);
    FUSHIA = vec3_new(255, 0, 137);
    ROSE = vec3_new(255, 170, 216);
    BLEU_CIEL = vec3_new(0, 213, 255);
    JAUNE = vec3_new(1, 247, 0);
    GOLD_ALBEDO = vec3_new(255, 211, 87);

}

/** @brief Regroupe toutes les initialisations à faire.
 */
void init_all(){
    init_colors();
}