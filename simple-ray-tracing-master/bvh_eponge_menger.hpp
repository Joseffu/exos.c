///@file Objet de la scène : une éponge de Menger optimisée

#ifndef BVH_EPONGE_MENGER_H
#define BVH_EPONGE_MENGER_H

#include "rayon.hpp"
#include "material.hpp"

#include "bvh_cube.hpp"
#include "sphere.hpp"
#include "metal_mat.hpp"

#include "BVH_noeud.hpp"
#include "eponge_menger.hpp"

#include <vector>

//TODO: fix bug ombres avec le cube optimisé
#define CUBE_OPTI_BVH false // indique s'il faut utiliser des cubes optimisés (bvh pour leurs 6 faces) ou non

class bvh_eponge_menger: public objet {
public:
    float cote; //largeur d'un côté

    //liste des sous élements
    //liste_objets *sous_eponges;    
    objet** sous_elements;
    liste_objets *sous_eponges;    

    BVH_noeud bvh;

    /** @brief Constructeur complet
     * @param centre_ Centre de l'éponge
     * @param cote_ longueur = hauteur = profondeur de l'éponge courante
     * @param niveau Niveau de la récursion : 1 = étape finale
     * @param mat_ Material de la surface de l'éponge
     */ 
    bvh_eponge_menger(vec3 centre_, float cote_, int niveau, material* m, std::vector<objet*> *sous_eponges_) {
        
        centre = centre_;
        cote = 0.95 * cote_; //pour laisser un espace entre les cubes 
        mat = m;

        float mc = cote / 2; //moitié d'un côté
        
        int nb_elements; //nombre de sous éléments de cette étape de ce niveau de l'éponge (1 ou 20 cubes)
        bool premier_appel = sous_eponges_ == NULL;
        //si premier appel de la récursion
        if(premier_appel) {
            //alors on créer la liste
            sous_eponges_ = new std::vector<objet*>;
        }
        //printf("niveau eponge : %d\n", niveau);

        //si on a atteint le dernier niveau alors cet objet est un simple cube
        if(niveau == 1) {

            /* TODO : trouver pourquoi la version bvh du cube donne un rendu des ombres plus clair */
            // note: essayer d'ajouter les faces du cubes directement la la liste sous_eponges_ ?
            if(CUBE_OPTI_BVH){
                sous_eponges_->push_back( new bvh_cube(
                    centre, cote, m
                ));
            } else {
                sous_eponges_->push_back( new cube(
                    centre, cote, m
                ));
            }


            nb_elements = 1;
        }
        //sinon cet objet est constitué de 20 autres sous cubes
        else {

            //pour chacun des 27 sous cubes
            for(int x = -1; x < 2; x++){
                for(int y = -1; y < 2; y++){
                    for(int z = -1; z < 2; z++){
                        float tc = cote / 3.0; //chaque cote du sous cube vaut 1/3 du cube de ce niveau
                        vec3 pos = vec3(
                            centre.x() + x * tc,
                            centre.y() + y * tc,
                            centre.z() + z * tc
                        );

                        int somme = abs(x) + abs(y) + abs(z);
                        //si c'est un sous cube de menger
                        if(somme > 1) {

                            //on créer la sous éponge
                            new bvh_eponge_menger(
                                pos, tc, niveau-1, mat, sous_eponges_
                            );
                        }
                        //on remplace les cubes centraux par des sphères réfléchissantes
                        else {
                            sous_eponges_->push_back( 
                                new sphere(
                                    pos, tc/5, 
                                    new metal_mat(
                                        vec3(0.5,0.5,0.5), 0
                                    )
                                )
                            );
                        }
                    }
                }
            }
        }

        //un seul bvh
        if(premier_appel){
            sous_eponges = new liste_objets( &((*sous_eponges_)[0]), sous_eponges_->size());
            printf("nb éléments éponge menger niveau %d : %d\n", niveau, (int)sous_eponges_->size());
            printf("Création BVH des élements de cette éponge.\n");
            bvh = BVH_noeud(0, sous_eponges_->size(), &((*sous_eponges_)[0]));
            if(CUBE_OPTI_BVH) printf("Note : les sous cubes de cette éponge utilisent un BVH pour leurs 6 faces.\n");
        }

        //calcul bornes boite collision
        vec3 c = centre;
        float r = cote /2; 
        coin_min = vec3( c.x() - r, c.y() - r, c.z() - r);  
        coin_max = vec3( c.x() + r, c.y() + r, c.z() + r); 

    }

    //fonction de collision. Voir la définition dans la classe objet pour plus de détails
    virtual bool collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const;
};



bool bvh_eponge_menger::collision(const rayon& ray, float dist_min, float dist_max, intersec_data& inter_data) const {
    //calcul de l'éventuelle intersection
   //return sous_eponges->collision(ray, dist_min, dist_max, inter_data);
   return bvh.collision(ray, dist_min, dist_max, inter_data);
}

#endif // BVH_EPONGE_MENGER_H
