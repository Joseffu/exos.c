#!/bin/bash

echo "* nettoyage des fichiers précédents"
rm max_char_value

echo "
* compilation du prog"
gcc max_char_value.c -o max_char_value -Wall -Wextra -Werror

echo "
* excecution du prog : ./max_char_value ... 
"
echo "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"
./max_char_value
echo "
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _"

echo "

* FIN exec run.sh"
